import React, { Component } from "react";
import {Link} from "react-router-dom"
import auth from "../services/authService"

export default class Menu extends Component {

  constructor(props) {
    super(props);
    this.state = {
      menu: false
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu(){
    this.setState({ menu: !this.state.menu })
  }

  render() {
    const user=auth.getUser();

  const show = (this.state.menu) ? "show" : "" ;

  return (

    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <a className="navbar-brand" href="/">Quiz</a>
      <button className="navbar-toggler" type="button" onClick={ this.toggleMenu }>
        <span className="navbar-toggler-icon"></span>
      </button>
      <div className={"collapse navbar-collapse " + show}>
        <div className="navbar-nav">
                    {!user&&<li className="nav-item"><Link className="nav-link" to="/takeQuiz">Take Quiz</Link></li>}
                    {user&&<li className="nav-item"><Link className="nav-link" to="/takeQuiz">Take Quiz Again</Link></li>}
                    {user && <li className="nav-item"><Link className="nav-link" to="/"><b>Hello,{user.name}</b></Link></li>}
                    {user && <li className="nav-item"><Link className="nav-link" to="/logout"><b>Logout</b></Link></li>}
        </div>
      </div>
    </nav>

  );
  }
}