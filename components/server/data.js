let ques=[
    {img:'https://image.shutterstock.com/image-vector/cartoon-elephant-isolated-on-white-600w-1008861691.jpg',optiona:'Rat',optionb:'Cat',optionc:'Elephant',optiond:'Tiger',correctoption:'Elephant'},
    {img:'https://image.shutterstock.com/image-vector/amur-tiger-goes-isolated-on-600w-1937998033.jpg',optiona:'Rat',optionb:'Cat',optionc:'Elephant',optiond:'Tiger',correctoption:'Tiger'},
    {img:'https://image.shutterstock.com/image-photo/scottish-straight-longhair-white-cat-600w-1007849866.jpg',optiona:'Rat',optionb:'Cat',optionc:'Elephant',optiond:'Tiger',correctoption:'Cat'},
    {img:'https://image.shutterstock.com/image-photo/rat-isolated-on-white-background-600w-536505532.jpg',optiona:'Rat',optionb:'Cat',optionc:'Elephant',optiond:'Tiger',correctoption:'Rat'},
    {img:'https://image.shutterstock.com/image-vector/giraffe-cartoon-style-insulated-on-600w-547760920.jpg',optiona:'Rat',optionb:'Cat',optionc:'Elephant',optiond:'Giraffe',correctoption:'Giraffe'},
];
let studentData=[
     {name:"Jack",totalques:5,correctques:3,wrongques:2},
];
module.exports={ques,studentData};