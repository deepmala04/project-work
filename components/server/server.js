var express=require("express");
var app=express();
app.use(express.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin,X-Requested-With,Content-Type,Accept");
  res.header("Access-Control-Allow-Methods","GET,POST,OPTIONS,PUT,PATCH,DELETE,HEAD");
  next();
});
const port=2410;
app.listen(port,()=>console.log("Listening on port:",port));
let {getConnection}=require("./db.js")
let {ques,studentData}=require("./data.js")
const pageSize=1;

let makeData = (pageNum, size, data1) => {
  let startIndex = (pageNum - 1) * size;
  let endIndex =
    data1.length > startIndex + size - 1
      ? startIndex + size - 1
      : data1.length - 1;
  let data2 = data1.filter(
    (lt, index) => index >= startIndex && index <= endIndex
  );
  let dataFull = {
    startIndex: data1.length > 0 ? startIndex + 1 : startIndex,
    endIndex: data1.length > 0 ? endIndex + 1 : endIndex,
    numOfItems: data1.length,
    data: data2,
  };
  return dataFull;
};


app.get('/quizquesall',function(req,res){
  let connection=getConnection();
  let query1='SELECT * FROM quiz';
  connection.query(query1,function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Retriving Data');
    }
    else res.send(result);
  })
})
app.get('/quizques',function(req,res){
  let page = req.query.page ? +req.query.page : 1;
  let connection=getConnection();
  let query1='SELECT * FROM quiz';
  connection.query(query1,function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Retriving Data');
    }
    else res.send(makeData(page,pageSize,result));
  })
})

app.get('/quizques/:id',function(req,res){
  let id=req.params.id;
  let connection=getConnection();
  let query1='SELECT * FROM quiz WHERE id=?';
  connection.query(query1,[id],function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Retriving Data');
    }
    else if(!result){
      res.status(404).send('Question Not Found');
    }
    else res.send(result[0]);
  })
})

app.post('/quizques',function(req,res){
  let body=req.body;
  let connection=getConnection();
  let query1='INSERT INTO quiz(img,optiona,optionb,optionc,optiond,correctoption) VALUES (?,?,?,?,?,?)';
  connection.query(query1,[body.img,body.optiona,body.optionb,body.optionc,body.optiond,body.correctoption],function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Inserting Data');
    }
    else res.send(`${result.affectedRows} Insertion Successful`);
  })
})

app.put('/editquizques/:id',function(req,res){
  let id=+req.params.id;
  let body=req.body;
  let connection=getConnection();
  let query1='UPDATE quiz SET img=?,optiona=?,optionb=?,optionc=?,optiond=?,correctoption=? WHERE id=?';
  connection.query(query1,[body.img,body.optiona,body.optionb,body.optionc,body.optiond,body.correctoption,id],function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error In Inserting Data');
    }
    else res.send(`${result.affectedRows} Updation Successful`);
  })
})

app.delete('/deletequizques/:id',function(req,res){
  let id=+req.params.id;
  let connection=getConnection();
  let query1='DELETE FROM quiz WHERE id=?';
  connection.query(query1,[id],function(err,result){
    if(err)
    {
       console.log(err);
       res.status(400).send("Error in deleting data");
    }
    else res.send(`${result.affectedRows} Deletion Successful`);
  })
})

app.get('/resetquizdata',function(req,res){
  let connection=getConnection();
  let query1='TRUNCATE TABLE quiz';
  connection.query(query1,function(err,result){
    if(err) {
      console.log(err);
      res.send("Error in deleting Data");
    }
    else{
      let quesArr=ques.map((q1)=>[q1.img,q1.optiona,q1.optionb,q1.optionc,q1.optiond,q1.correctoption]);
      let query2='INSERT INTO quiz(img,optiona,optionb,optionc,optiond,correctoption) VALUES ?';
       connection.query(query2,[quesArr],function(err,result){
       if(err) {
         console.log(err);
         res.status(400).send("Error in inserting Data");
        }
       else res.send(`${result.affectedRows} Insertion Successful`);
      })
    }
    })
})
app.get('/studentdata',function(req,res){
  let page = req.query.page ? +req.query.page : 1;
  let connection=getConnection();
  let query1='SELECT * FROM studentdata';
  connection.query(query1,function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Retriving Data');
    }
    else res.send(result);
  })
})

app.get('/studentdata/:name',function(req,res){
  let name=req.params.name;
  let connection=getConnection();
  let query1='SELECT * FROM studentdata WHERE name=?';
  connection.query(query1,[name],function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Retriving Data');
    }
    else if(!result){
      res.status(404).send('Student Not Found');
    }
    else res.send(result[0]);
  })
})

app.post('/studentdata',function(req,res){
  let body=req.body;
  let u1 = studentData.find((u) => body.name === u.name);
  let connection=getConnection();
  let query1='INSERT INTO studentdata(name,totalques,correctques,wrongques) VALUES (?,?,?,?)';
  if(!u1){
  connection.query(query1,[body.name,body.totalques,body.correctques,body.wrongques],function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error in Inserting Data');
    }
    else res.send(`${result.affectedRows} Insertion Successful`);
  })
}
else{
  res.status(400).send('Student already Exists')
}
})
app.put('/editstudentdata/:id',function(req,res){
  let id=+req.params.id;
  let body=req.body;
  let connection=getConnection();
  let query1='UPDATE studentdata SET name=?,totalques=?,correctques=?,wrongques=? WHERE id=?';
  connection.query(query1,[body.name,body.totalques,body.correctques,body.wrongques,id],function(err,result){
    if(err) {
      console.log(err);
      res.status(400).send('Error In Inserting Data');
    }
    else res.send(`${result.affectedRows} Updation Successful`);
  })
})

app.delete('/deletestudentdata/:id',function(req,res){
  let id=+req.params.id;
  let connection=getConnection();
  let query1='DELETE FROM studentdata WHERE id=?';
  connection.query(query1,[id],function(err,result){
    if(err)
    {
       console.log(err);
       res.status(400).send("Error in deleting data");
    }
    else res.send(`${result.affectedRows} Deletion Successful`);
  })
})

app.get('/resetstudentdata',function(req,res){
  let connection=getConnection();
  let query1='TRUNCATE TABLE studentdata';
  connection.query(query1,function(err,result){
    if(err) {
      console.log(err);
      res.send("Error in deleting Data");
    }
    else{
      let stArr=studentData.map((q1)=>[q1.name,q1.totalques,q1.correctques,q1.wrongques]);
      let query2='INSERT INTO studentdata(name,totalques,correctques,wrongques) VALUES ?';
       connection.query(query2,[stArr],function(err,result){
       if(err) {
         console.log(err);
         res.status(400).send("Error in inserting Data");
        }
       else res.send(`${result.affectedRows} Insertion Successful`);
      })
    }
    })
})
