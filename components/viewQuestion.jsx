import React,{Component} from "react";
import {Link} from "react-router-dom";
import queryString from "query-string";
import http from "../services/httpService"
import auth from "../services/authService";
class ViewQuestion extends Component{
    state={
        questions:[],
        student:{totalattempted:"0",correctques:"0",wrongques:"0"},
        studentdata:{},
        count:0,
        view:0,
        errors:"",
    }
    async fetchData(){
        const user=auth.getUser();
        let queryParams=queryString.parse(this.props.location.search);
        let str="";
        let response;
        str=this.makeSearchString(queryParams)
        if(str){
            response=await http.get(`https://peaceful-meadow-76488.herokuapp.com/quizques?${str}`);
        }
        else{
            response=await http.get(`https://peaceful-meadow-76488.herokuapp.com/quizques`);           
        }
        let {data}=response;
        this.setState({questions:data});
    }
    async getStData(){
        const user=auth.getUser();
        let response=await http.get(`https://peaceful-meadow-76488.herokuapp.com/studentdata/${user.name}`);           
        let {data}=response;
        if(data)
        this.setState({studentdata:data});
        else
        this.setState({student:{totalattempted:"0",correctques:"0",wrongques:"0"}})
    }
    componentDidMount(){
        this.fetchData()
        this.getStData();
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(prevProps!==this.props) {
            this.fetchData();
        }
    
    }
    async putData(url,obj){
           let response=await http.put(url,obj);
    }
    handleClick=(index,str)=>{
        let s1={...this.state}
        console.log(s1.studentdata)
       // let totalArr=s1.student.totalattempted.split(',');
       // let correctArr=s1.student.correctques.split(',');
       // let wrongArr=s1.student.wrongques.split(',');
        if(s1.questions.numOfItems-1===s1.count){
            if(str===s1.questions.data[index].correctoption){
            //totalArr[totalArr.length-1]=+totalArr[totalArr.length-1]+1;
            // correctArr[correctArr.length-1]=+correctArr[correctArr.length-1]+1;
            s1.student.totalattempted=+s1.student.totalattempted+1;
            s1.student.correctques=+s1.student.correctques+1;    
            s1.view=1
            s1.errors="";
            s1.studentdata.totalattempted=s1.studentdata.totalattempted+s1.student.totalattempted+",";
            s1.studentdata.correctques=s1.studentdata.correctques+s1.student.correctques+",";
            s1.studentdata.wrongques=s1.studentdata.wrongques+s1.student.wrongques+",";
            s1.studentdata.noofattempt=+s1.studentdata.noofattempt+1;
              this.putData(`https://peaceful-meadow-76488.herokuapp.com/editstudentdata/${s1.studentdata.id}`,this.state.studentdata)
            }
            else{
                s1.errors="";
                s1.student.totalattempted=+s1.student.totalattempted+1;
                s1.student.wrongques=+s1.student.wrongques+1;    
               
            //totalArr[totalArr.length-1]=+totalArr[totalArr.length-1]+1;
            //wrongArr[wrongArr.length-1]=+wrongArr[wrongArr.length-1]+1;
            s1.errors="Invalid Option : "+str+" .Please Choose correct option";
            }

        }
       else 
       {
        if(str===s1.questions.data[index].correctoption)
        {
        s1.count+=1;
        let queryParams=queryString.parse(this.props.location.search);
        let {page=1}=queryParams;
        let newPage=+page + 1;
        queryParams.page=newPage
        s1.student.totalattempted=+s1.student.totalattempted+1;
        s1.student.correctques=+s1.student.correctques+1;    
       
        //if(s1.count){
           // totalArr[totalArr.length-1]=+totalArr[totalArr.length-1]+1;
            //correctArr[correctArr.length-1]=+correctArr[correctArr.length-1]+1;
            
        //}
        //else{
        //totalArr.push("1");
        //correctArr.push("1");
        //s1.count=1;
        //}
        s1.errors="";
        this.callURL("/viewquiz",queryParams); 
        }
       else{
        s1.errors="";
        s1.student.totalattempted=+s1.student.totalattempted+1;
        s1.student.wrongques=+s1.student.wrongques+1;    
       
        //if(s1.count2){
        //totalArr[totalArr.length-1]=+totalArr[totalArr.length-1]+1;
        //wrongArr[wrongArr.length-1]=+wrongArr[wrongArr.length-1]+1;
        //}
       // else{
            //totalArr.push("1");
            //wrongArr.push("1");
            //s1.count2=1
            //}
       s1.errors="Invalid Option : "+str+" .Please Choose correct option";
    }
    }
    //s1.student.totalattempted=totalArr.join(',')
    //s1.student.correctques=correctArr.join(',')
   // s1.student.wrongques=wrongArr.join(',')
    this.setState(s1);
    }
    
    callURL =(url,options)=>{
        let searchString=this.makeSearchString(options);
        this.props.history.push({pathname:url,search:searchString,})
    }
    makeSearchString=(options)=>{
        let {page=1}=options;
        let searchStr="";
        searchStr=this.addToQueryString(searchStr,"page",page)
        return searchStr;
    }
    addToQueryString=(str,paramName,paramValue)=>
    paramValue?str?`${str}&${paramName}=${paramValue}`:`${paramName}=${paramValue}`:str
    
    handleScore=()=>{
        let s1={...this.state};
        s1.view=1;
        this.setState(s1);
    }
    render(){
        const {data}=this.state.questions;
        const {studentdata,view,errors}=this.state;
        let student={...studentdata};
        console.log(this.state.studentdata)
        console.log(this.state.student)
        return (
            <div className="container text-center">
              <h3>Choose correct option</h3>
              {view?
               <React.Fragment>
                   <div className="row">
                   <div className="col-12 border bg-light">
                   <h2 className="text-success" width="100%">Name:{student.name}<br/></h2>
                   <h3>Total Ques:{student.totalques}</h3>
                   <h3>Total Display:{this.state.questions.numOfItems}</h3>
                   <h3>No Of Attempts:{student.noofattempt?student.noofattempt:"1"}<br/></h3>
                   <h3>Total Attempted:{student.totalattempted?student.totalattempted:"1"}<br/>
                   Correct Attempted:{student.correctques?student.correctques:"0"}<br/></h3>
                   <h3 className="text-danger">Wrong Attempted:{student.wrongques?student.wrongques:"0"}<br/></h3>
                   </div>
                   </div>
              </React.Fragment>
              :
              data?
                data.length>0?
                  data.map((q1,index)=>(
                  <div className="row">
                    <h4 className="text-dark bg-danger">{errors?errors:""}</h4>
                  <div className="col-lg-3"></div>
                  <div className="col-lg-6 col-sm-12">
                    <img src={q1.img} width="100%" height="400vh"/>
                    <button className="btn btn-primary m-2" onClick={()=>this.handleClick(index,q1.optiona)}>{q1.optiona}</button>
                    <button className="btn btn-primary m-2" onClick={()=>this.handleClick(index,q1.optionb)}>{q1.optionb}</button>
                    <button className="btn btn-primary m-2" onClick={()=>this.handleClick(index,q1.optionc)}>{q1.optionc}</button>
                    <button className="btn btn-primary m-2" onClick={()=>this.handleClick(index,q1.optiond)}>{q1.optiond}</button>
                  </div>
                 <div className="col-lg-3"></div>
                 </div>
                ))
                :
                <React.Fragment>
                <h2>No Question</h2>
                <button className="btn btn-success" onClick={()=>this.handleScore()}>View Score</button>
                </React.Fragment>
              :""}
            </div>
        )
    }
}
export default ViewQuestion;