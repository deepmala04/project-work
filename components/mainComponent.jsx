import React,{Component} from "react";
import {Route,Switch,Redirect} from "react-router-dom";
import Welcome from "./welcome";
import Navbar from "./navbar2"
import ViewQuestion from "./viewQuestion";
import TakeQuiz from "./takequiz";
import Logout from "./logout";
class MainComponent extends Component{
    render(){
        return (
            <div className="container">
                <Navbar/>
                <Switch>
                    <Route path="/viewquiz" component={ViewQuestion}/>
                    <Route path="/takequiz" component={TakeQuiz}/>
                    <Route path="/welcome" component={Welcome}/>
                    <Route path="/logout" component={Logout}/>
                    <Redirect from="/" to="/welcome"/>
                    </Switch>
            </div>
        )
    }
}
export default MainComponent;