import React,{Component} from "react";
import http from "../services/httpService"
import auth from "../services/authService"
class TakeQuiz extends Component{
    state={
    questions:[],
    data:{name:"",totalques:"",totalattempted:"",correctques:"",wrongques:"",noofattempt:""},
    errors:{},
    };
    async fetchData(){
        let response=await http.get(`https://peaceful-meadow-76488.herokuapp.com/quizquesall`);
        let {data}=response;
        this.setState({questions:data});
    }
    componentDidMount(){
        this.fetchData()
    }
    componentDidUpdate(prevProps,prevState)
    {
        if(prevProps!==this.props) this.fetchData();
    
    }
    handleChange=(e)=>{
    const {currentTarget:input}=e;
    let s1={...this.state}
    s1.data[input.name]=input.value;
    this.setState(s1)
    };

    async postData(url,obj){
        //try{
        let response=await http.post(url,obj);
        let {data}=response;
        console.log(data)
        if(data.id){
           this.setState({data:{name:data.name,totalques:data.totalques,totalattempted:data.totalattempted,correctques:data.correctques,wrongques:data.wrongques,noofattempt:data.noofattept}})
         }
        auth.login({name:obj.name})
        window.location="/viewquiz";
        //}
    //catch(ex){
        //if(ex.response && ex.response.status===400){
           // let errors={}
           // errors.id=ex.response.data;
           // this.setState({errors:errors});
       // }
    //}

    }
    handleSubmit=(e)=>{
        e.preventDefault();
        const {data,questions}=this.state;
        data.totalques=questions.length;
        this.postData("https://peaceful-meadow-76488.herokuapp.com/studentdata",data);
    }
    render(){
        const user=auth.getUser();
        let {name}=this.state.data;
        let {errors}=this.state;
        return (
            <div className="container">
                {user? window.location="/viewquiz"
                :<div className="row">
                    <div className="col-3"></div>
                    <div className="col-6">
                    <h4>Add Your Name</h4>
                    <h6 className="text-dark bg-danger">{errors?errors.id:""}</h6>
                     <div className="form-group">
                            <label>Name</label>
                            <input type="text" className="form-control" id="name" name="name" placeholder="Enter Name" value={name} onChange={this.handleChange}/><br/>
                            <button className="btn btn-primary" onClick={this.handleSubmit}>Add</button>
                        </div>
                    </div>
                    <div className="col-3"></div>
                </div>}
            </div>
        )
    }
}
export default TakeQuiz;