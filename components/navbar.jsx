import React,{ Component } from "react";
import {Link} from "react-router-dom"
import auth from "../services/authService"
class Navbar extends Component {
    render(){
        const user=auth.getUser();
        return (
            <nav className="navbar navbar-expand-lg navbar-light bg-info navbar-inverse">
                <Link className="navbar-brand text-success" to="/">Quiz</Link>
                    <ul className="nav navbar-nav mr-auto">
                                {!user&&<li className="nav-item"><Link className="nav-link" to="/takeQuiz">Take Quiz</Link></li>}
                                {user&&<li className="nav-item"><Link className="nav-link" to="/takeQuiz">Take Quiz Again</Link></li>}
                                {user && <li className="nav-item"><Link className="nav-link" to="/"><b>Hello,{user.name}</b></Link></li>}
                                {user && <li className="nav-item"><Link className="nav-link" to="/logout"><b>Logout</b></Link></li>}
                            </ul>
            </nav>
        )
    }
}
export default Navbar;